package com.wyn.storm;

import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import backtype.storm.tuple.Fields;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.Iterator;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class GeneratorSpout extends BaseRichSpout {
	private static final Logger LOG = LoggerFactory.getLogger(GeneratorSpout.class);

	private SpoutOutputCollector collector;
    
    private LinkedBlockingQueue<Values> queue;
    private DataFileReader fileReader;
    private Thread readerThread;

	@SuppressWarnings("rawtypes")
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {

        this.collector = collector;
        
        this.queue = new LinkedBlockingQueue<Values>();

        this.fileReader = new DataFileReader(queue);
        this.readerThread = new Thread(fileReader);
        this.readerThread.start();
	}

    int currentRecord = 1;

	public void nextTuple() {

        Values recordValues = this.queue.poll();
        if (null == recordValues) {
            Utils.sleep(500); // Back off
        } else {
            LOG.debug("Received(poll) tuple # " + currentRecord + ", values: " + recordValues);
            this.collector.emit(recordValues);
            currentRecord++;
            LOG.debug("Exiting nextTuple, currentRecord incremented to: " + currentRecord);
        }
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("id", "GNR_NUM", "CONFIRMATION_NO", "CANCELLATION_NO", "BRAND_ID", "SITE_ID", 
                        "MEMBER_NUM", "BOOKING_TS", "RESV_STATUS", "CHANNEL", "CHECK_IN_DATE", "LOS", 
                        "POINTS_USED", "TOTAL_TAX", "NO_OF_ROOMS", "AWARD_NUMBER", "ADULTS", "CHILDREN", 
                        "CHILDREN_18", "GUEST_FIRST_NAME", "GUEST_MIDDLE_NAME", "GUEST_LAST_NAME", 
                        "GUEST_ADDRESS", "CITY", "STATE", "COUNTRY", "POSTAL_CODE", "RATE_PLAN", 
                        "ROOM_TYPE", "ROOM_REVENUE", "ROOM_TITLE", "RATE_SUPPRESSION_FLAG")); //"GUEST_NAME", "src", "ind_ts"));
	}

    public String getUUID() {
        return UUID.randomUUID().toString();
    }
    public String getRandomString(int len) {
        SecureRandom rs = new SecureRandom();
        int maxIterations=1000;
        String s="";
        int i=0;
        while(s.length()<len) {
            s+=(String) new BigInteger(230, rs).toString(len);
            i++;
        }
        return s.substring(0, len);
    }
}

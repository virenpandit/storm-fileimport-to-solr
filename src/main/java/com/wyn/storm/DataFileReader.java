package com.wyn.storm;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Map;
import java.nio.charset.Charset;
import java.util.UUID;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.io.*;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import backtype.storm.tuple.Values;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataFileReader implements Runnable {
	private static final Logger LOG = LoggerFactory.getLogger(DataFileReader.class);

    private static final String DATA_FILE = "/home/pandivir/storm-fileimport-to-solr/data/sourcefile.csv";

    private static final long SKIP_ROWS = 2;
    
    LinkedBlockingQueue queue;

    public DataFileReader(LinkedBlockingQueue queue) {
        this.queue = queue;
    }

    public void run() {

        LOG.debug("Inside run()");
        
        try {
            CsvSchema bootstrap = CsvSchema.emptySchema().withHeader();
            bootstrap.withSkipFirstDataRow(true);
            CsvSchema.Builder builder = bootstrap.rebuild();
            builder.setSkipFirstDataRow(false);
            bootstrap = builder.build();
            LOG.info("Schema generated");
            CsvMapper csvMapper = new CsvMapper();
            MappingIterator<Map<?, ?>> mappingIterator = csvMapper.reader(Map.class).with(bootstrap).readValues(new File(DATA_FILE));

            long rowid = 0;
            long groupid = 0;
            if(!mappingIterator.hasNext()) {
                LOG.error("*** ERROR: NO RECORDS IN FILE: " + DATA_FILE);
            } else {
                while (mappingIterator.hasNext()) {
                    rowid++;
                    LOG.info("RowId: " + rowid);
                    // if(rowid<SKIP_ROWS) {
                        // try {
                            // mappingIterator.next();
                        // } catch(Exception ex) {
                            // LOG.error("Exception skipping row[" + rowid + "]: ", ex);
                        // }
                    // } else {
                        Map map = (Map<String, String>)mappingIterator.nextValue();
                        Iterator<String> fieldsIterator = map.keySet().iterator();
                        String tmp = "";
                        while(fieldsIterator.hasNext()) {
                            String fieldName = fieldsIterator.next();
                            tmp += fieldName + " ";
                        }
                        LOG.info("Fields: " + tmp);

                        String GNR_NUM = getFieldValueOrEmpty(map, "GNR_NUM");
                        String CONFIRMATION_NO = getFieldValueOrEmpty(map, "CONFIRMATION_NO");
                        String CANCELLATION_NO = getFieldValueOrEmpty(map, "CANCELLATION_NO");
                        String BRAND_ID = getFieldValueOrEmpty(map, "BRAND_ID");
                        String SITE_ID = getFieldValueOrEmpty(map, "SITE_ID");
                        String MEMBER_NUM = getFieldValueOrEmpty(map, "MEMBER_NUM");
                        
                        String BOOKING_TS = getFieldValueOrEmpty(map, "BOOKING_TS");
                        BOOKING_TS = getFormattedDate(BOOKING_TS);

                        String RESV_STATUS = getFieldValueOrEmpty(map, "RESV_STATUS");
                        String CHANNEL = getFieldValueOrEmpty(map, "CHANNEL");

                        String CHECK_IN_DATE = getFieldValueOrEmpty(map, "CHECK_IN_DATE");
                        CHECK_IN_DATE = getFormattedDate(CHECK_IN_DATE);

                        String LOS = getFieldValueOrEmpty(map, "LOS");
                        String POINTS_USED = getFieldValueOrEmpty(map, "POINTS_USED");
                        String TOTAL_TAX = getFieldValueOrEmpty(map, "TOTAL_TAX");
                        String NO_OF_ROOMS = getFieldValueOrEmpty(map, "NO_OF_ROOMS");
                        String AWARD_NUMBER = getFieldValueOrEmpty(map, "AWARD_NUMBER");
                        String ADULTS = getFieldValueOrEmpty(map, "ADULTS");
                        String CHILDREN = getFieldValueOrEmpty(map, "CHILDREN");
                        String CHILDREN_18 = getFieldValueOrEmpty(map, "CHILDREN_18");
                        String GUEST_FIRST_NAME = getFieldValueOrEmpty(map, "GUEST_FIRST_NAME");
                        String GUEST_MIDDLE_NAME = getFieldValueOrEmpty(map, "GUEST_MIDDLE_NAME");
                        String GUEST_LAST_NAME = getFieldValueOrEmpty(map, "GUEST_LAST_NAME");
                        String GUEST_ADDRESS = getFieldValueOrEmpty(map, "GUEST_ADDRESS");
                        String CITY = getFieldValueOrEmpty(map, "CITY");
                        String STATE = getFieldValueOrEmpty(map, "STATE");
                        String COUNTRY = getFieldValueOrEmpty(map, "COUNTRY");
                        String POSTAL_CODE = getFieldValueOrEmpty(map, "POSTAL_CODE");
                        String RATE_PLAN = getFieldValueOrEmpty(map, "RATE_PLAN");
                        String ROOM_TYPE = getFieldValueOrEmpty(map, "ROOM_TYPE");
                        String RATE_SUPPRESSION_FLAG = getFieldValueOrEmpty(map, "RATE_SUPPRESSION_FLAG");
                        String ROOM_REVENUE = getFieldValueOrEmpty(map, "ROOM_REVENUE");
                        String ROOM_TITLE = getFieldValueOrEmpty(map, "ROOM_TITLE");
                        
                        String ID = getUUID();

                        Values recordValues = new Values(GNR_NUM, GNR_NUM, CONFIRMATION_NO, CANCELLATION_NO, BRAND_ID, SITE_ID, 
                                                    MEMBER_NUM, BOOKING_TS, RESV_STATUS, CHANNEL, CHECK_IN_DATE, LOS, 
                                                    POINTS_USED, TOTAL_TAX, NO_OF_ROOMS, AWARD_NUMBER, ADULTS, CHILDREN, 
                                                    CHILDREN_18, GUEST_FIRST_NAME, GUEST_MIDDLE_NAME, GUEST_LAST_NAME, 
                                                    GUEST_ADDRESS, CITY, STATE, COUNTRY, POSTAL_CODE, RATE_PLAN, 
                                                    ROOM_TYPE, ROOM_REVENUE, ROOM_TITLE, RATE_SUPPRESSION_FLAG); //, src, ind_ts);
                                                    
                        LOG.info("Returning >> Row # " + rowid + ", ID: " + ID + ", GNR_NUM: " + GNR_NUM);

                        queue.offer(recordValues);
                    //}
                }
            }

            LOG.debug("Exiting run()");

        } catch(Exception ex) {
            LOG.error("Exception in DataFileReader.run()", ex);
        }
    }
    
    private String getFormattedDate(String dateStr) {
        Date date = new Date(dateStr);
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss a");
        sdf.applyPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        return sdf.format(date);
    }

    private String getFieldValueOrEmpty(Map<String,String> map, String key) {
        String value = "(null)";
        if(map.containsKey(key)) {
            value = map.get(key);
        }
        if(null == value) value = "(null)";

        return value;
    }


    public String getUUID() {
        return UUID.randomUUID().toString();
    }
}
